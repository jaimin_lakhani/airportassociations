/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author jaiminlakhani
 */
public class Airline {
    private String name;
    private String shortCode;
    
    public Airline(String name, String shortCode) {
        this.name = name;
        this.shortCode = shortCode;
    }
    
    public String getName(String name){
        return this.name;
    }
    
    public void setName(String name){
        this.name = name;
    }
    
    public String getShortCode(){
        return this.shortCode;
    }
    
    public void setShortCode(String shortcode){
        this.shortCode = shortCode;
    }
}
