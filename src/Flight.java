
import java.util.Date;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author jaiminlakhani
 */
public class Flight {
   private int number;
   private Date date;
   
   public Flight(int number, Date date) {
       this.number = number;
       this.date = date;
   }
   
   public int getNumber() {
       return this.number;
   }
   
   public void setNumber(int number){
       this.number = number;
   }
   
   public Date getDate() {
       return this.date;
   }
   
   public void setDate(Date date){
       this.date = date;
   }
}
