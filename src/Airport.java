/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */


/**
 *
 * @author jaiminlakhani
 */
public class Airport {
    private String code;
    private String city;
    
    public Airport(String code, String city){
        this.code = code;
        this.city = city;
    }
    
    public String getCode() {
        return this.code;
    }
    
    public void setCode(String code){
        this.code = code;
    }
    
    public String city(){
        return this.city;
    }
    
    public void city(String city){
        this.city = city;
    }
}
