/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author jaiminlakhani
 */
public class Airplane {
    private int id;
    private int numberOfSeats;
    private String make;
    private String model;
    
    public Airplane(int id, int numberOfSeats, String make, String model){
        this.id = id;
        this.numberOfSeats = numberOfSeats;
        this.make = make;
        this.model = model;
    }
    
    public int getId() {
        return this.id;
    }
    
    public void setId(){
        this.id = id;
    }
    
    public int getNumberOfSeats() {
        return this.numberOfSeats;
    }
    
    public void setNumberOfSeats(int numberOfSeats){
        this.numberOfSeats = numberOfSeats;
    }
    
    public String getMake() {
        return this.make;
    }
    
    public void setMake(String make){
        this.make = make;
    }
    
    public String getModel() {
        return this.model;
    }
    
    public void setModel(String model){
        this.model = model;
    }
}
